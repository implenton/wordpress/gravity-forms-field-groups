<?php
/*
 * Plugin Name:       Gravity Forms Field Groups
 * Description:       Group field using the default Section Break field: useful if you want multiple columns
 * Plugin URI:        https://implenton.com/
 * Version:           1.1
 * Author:            implenton
 * Author URI:        https://implenton.com
 * License:           GPLv3
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.txt
 * GitLab Plugin URI: implenton/wordpress/gravity-forms-field-groups
 */

namespace GFFG;

if ( ! defined( 'WPINC' ) ) {
    die;
}
add_filter( 'gform_pre_render', __NAMESPACE__ . '\set_divider_field_type_based_on_classes' );
add_filter( 'gform_field_container', __NAMESPACE__ . '\replace_field_container_html', 10, 6 );

function replace_field_container_html( $field_container, $field, $form, $css_class, $style, $field_content ) {
    if ( is_admin() ) {
        return $field_container;
    }

    $base_class = apply_filters( 'gffg/group/class', 'gfield_group' );

    if ( $field['type'] == 'groupStart' ) {
        $field_container_class = str_replace( apply_filters( 'gffg/start/class', 'group-start' ), $base_class, $css_class );
        $field_container       = sprintf( '<li id="field_%1$s_%2$s" class="%3$s"><div class="%4$s__container"><ul class="%4$s__list">', $form['id'], $field['id'], $field_container_class, $base_class );
        $field_container       = apply_filters( 'gffg/start/markup', $field_container, $field, $form, $css_class, $style, $field_content );
    }

    if ( $field['type'] == 'groupEnd' ) {
        $field_container = '</ul></div></li>';
        $field_container = apply_filters( 'gffg/end/markup', $field_container, $field, $form, $css_class, $style, $field_content );
    }

    return $field_container;
}

function set_divider_field_type_based_on_classes( $form ) {
    foreach ( $form['fields'] as $field ) {
        if ( $field['type'] == 'section' && strpos( $field['cssClass'], apply_filters( 'gffg/start/class', 'group-start' ) ) !== false ) {
            $field['type'] = 'groupStart';
        } elseif ( $field['type'] == 'section' && strpos( $field['cssClass'], apply_filters( 'gffg/end/class', 'group-end' ) ) !== false ) {
            $field['type'] = 'groupEnd';
        }
    }

    return $form;
}
