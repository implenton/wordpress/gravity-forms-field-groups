# Gravity Forms Field Groups

*Credits:* This is heavily inspired and it is fork of [Multiple Columns for Gravity Forms](https://wordpress.org/plugins/gf-form-multicolumn/).

## How to use it

1. Wrap the fields in two [Sections Breaks](https://docs.gravityforms.com/section-break/)
2. Add the `group-start` CSS class to the first Section Break ("starting") 
3. Add the `group-end` CSS class to the second Section Break ("closing")

The CSS classes are added in the [Appearance tab](https://docs.gravityforms.com/section-break/#appearance).

## How is this different from Multiple Columns for Gravity Forms?

This fork favours developers instead of hobbyist:

- There is no CSS file
- Inline CSS is not calculated and generated
- The markup is simpler and CSS classes are kept to minimum
- The logic is more straightforward and easier to understand

Keep in mind that by removing features this plugin "does *not work out of the box*".
You have to write the CSS if you want to have multiple columns. 

## Customization

#### Change the indicator classes

There is very slim chance that you are using the default `group-start` and `group-end` classes for something else than targeting the Section Breaks.
If you do and this causes you issues you can use these filters to change them:

- `gffg/start/class` 
- `gffg/end/class` 

#### Change the group base class

If you take a good look at the generated markup

```html
<li id="…" class="gfield gfield_group …">
    <div class="gfield_group__container">
        <ul class="gfield_group__list">
            <!-- the wrapped fields -->
        </ul>
    </div>
</li>
```

you will notice that `gfield_group` class is the "base" class. Use the `gffg/group/class` filter to change this.

#### Customize the markup

You have two filters if you need granular control over the wrapper's markup:

- `gffg/start/markup`
- `gffg/end/markup`
